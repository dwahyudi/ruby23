FROM drecom/ubuntu-ruby:2.3.1

RUN export LANG=en_US.UTF-8
RUN export LANGUAGE=en_US.UTF-8

RUN apt-get update -qq && apt-get install -y git build-essential libpq-dev mysql-client apt-transport-https ca-certificates unzip xvfb cmake jq curl pwgen
RUN apt-get install -y --allow-unauthenticated redis-server